#!/usr/bin/env python3
from app import create_app
import re

app = create_app(debug=True)

app.secret_key = 'asdasdasdas6d9hahd98ad78ab6sd'
app.jinja_env.auto_reload = True

@app.template_filter('cleanhtml')
def cleanhtml(raw_html):
  cleanr = re.compile('<.*?>')
  cleantext = re.sub(cleanr, '', raw_html)
  return cleantext

@app.template_filter('summary')
def summary(value):
	addon = ""
	if len(value) > 100:
		addon = '...'
	return value[:100]+addon
@app.template_filter('formatdatetime')
def format_datetime(value, format="%d %b %Y %I:%M %p"):
    if value is None:
        return ""
    return value.strftime(format)	

app.run(host="0.0.0.0")

from app.controllers import HomeController,AdminController, NewsController,ContentController

home_controller = HomeController()
admin_controller = AdminController()
news_controller = NewsController()
content_controller = ContentController()

from flask import render_template, session
from app.mongo import News, Menu
import bcrypt
import time

class HomeController:

	def update_featured(self, data):
		code = data.get('data[code]')
		News.mongo(self.database).update_many({},{"$set":{'featured': 'off'}})
		time.sleep(1)
		News.mongo(self.database).update_one({'slug':code}, {"$set":{'featured': 'on'}})
	
	def make_comment(self, code, data):
		News.mongo(self.database).update({'slug' : code}, { "$push" : {'comments': {'comment' : data.get('comment')} }})

	def detail(self, code):
		current = None
		n = News.mongo(self.database).find({'slug':code})
		if n:
			current = list(n)[0]
		return render_template('detail.html', current=current, placeholder='Noticias, jugadores, torneos, etc.')

	def view(self, query):
		search = query.get('query',False)

		list_menu = Menu.getAll(self.database, False)
		News.mongo(self.database).create_index([('description', 'text')])
		featured_new = News.mongo(self.database).find_one({'featured':'on'})
		if search:
			news = list(News.mongo(self.database).find({"$text": { "$search": search }}))
		else:
			news = list(News.getAll(self.database, True,{"featured":"off"}))

		return render_template('index.html',list_menu=list_menu,featured_new=featured_new, news = news,
			placeholder='Noticias, jugadores, torneos, etc.')

class AdminController:

	def attempt(self, username, password):
		pswd = bcrypt.hashpw(bytes(password.encode('utf-8')), bcrypt.gensalt())
		us= self.database.db.users.find_one({"username":username})
		if us:
			if bcrypt.checkpw(password.encode('utf-8'), us['password']):
				return { 'status':True, 'message':'ok'}
			else:
				return { 'status':False, 'message':'invalid password'}
		else:
			{ 'status':False, 'message':'User doesnt exist'}

	def register(self, username, password):
		pswd = bcrypt.hashpw(bytes(password.encode('utf-8')), bcrypt.gensalt())
		return self.database.db.users.insert({"username":username, "password":pswd})

	def login(self):
		return render_template('admin/index.html',
			news = list(News.getAll(self.database)),
			placeholder='Noticias, jugadores, torneos, etc.')

	def dashboard(self):
		return render_template('admin/dashboard.html')

class NewsController:

	def edit_view(self, code):
		current = None
		return render_template('admin/news/add_edit.html', current=code)
	
	def save_item(self, data):
		n = News.form(data)
		n.save(self.database)
		return render_template('admin/news/add_edit.html')

	def add_view(self):
		model = News.form()
		return render_template('admin/news/add_edit.html', model=model)

	def view(self):
		news = list(News.getAll(self.database))
		return render_template('admin/news/list.html', news=news)

class ContentController:

	def add_menu(self, data):
		m = Menu()
		m.name = data.get('name')
		m.url = data.get('url')
		m.save(self.database)
		return {'status' : True}
	def view(self):
		list_menu = Menu.getAll(self.database)
		return render_template('admin/content/index.html', list_menu = list(list_menu) )

var helper = {
	fetch : function(url, callback, data){
		if(typeof data == 'undefined'){
			//GET
			$.ajax({
				url : url,
				type : 'GET',
				success : function(response){
					console.log(response)
				}
			})
		}else{
			//POST
			$.ajax({
				url : url,
				type : 'POST',
				data : data,
				success : function(response){
					console.log(response)
				}
			})
		}
	}
}
var News = function(){
	this.featured= function(){
		var ck = $('.ck-featured .custom-control-input');
		ck.on('change', function(){
			var me = $(this);
			helper.fetch(KING.url.featured, function(response){
				console.log(response)
			}, {data:me.data(), value:me.val()} );
		});
	}
	this.save = function(){

		var f = $('#news-form');
		var m = $('#response-message');
		f.on('submit', function(){
			var me = $(this);
			var d = me.serializeArray();
			console.log(d)
			if(d[1].value.length <= 0||d[2].value.length<=0){
				$('#response-message').html('Completa todos los campos');
				return false;
			}
		});


	}
	return this;
}


var App = {
	init : function(){
		var n = News();
		n.featured();
		n.save();
	}
}

$(App.init)
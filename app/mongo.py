from pymongo import MongoClient
import datetime
import pymongo
import random
import string

def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

def attrs(cls):
	return [(x, cls.__dict__[x]) for x in cls.__dict__.keys() if not x.startswith('_')]
		
class MongoConnection():
	db = None
	def __init__(self):
		c = MongoClient('db')
		self.db = c.db_chess

class Document:
	
	created_at=None
	String = 0
	Boolean = 1
	TextField = 'text'
	TextArea = "area"
	Checkbox = "checkbox"

	@classmethod
	def getAll(cls,mongo, order=True, f={}):
		return mongo.db[cls._table].find(f).sort("created_at", pymongo.DESCENDING if order else pymongo.ASCENDING)


	@classmethod
	def mongo(cls,mongo):
		return mongo.db[cls._table]


	@classmethod
	def form(cls, data=None):
		if data == None:
			return attrs(cls)
		##else
		rt = data.to_dict(flat=True)
		n = News()
		for k, val in attrs(cls):
			if hasattr(n, k) and k in rt:
				setattr(n, k, rt[k])
			else:
				setattr(n, k, val['value'])
		return n

	def save(self, mongo):
		db = mongo.db

		self.created_at = datetime.datetime.now()
		self.status = 1
		self.slug = randomString(10)
		
		data = {}
		for y in attrs(self):
			data[y[0]] = y[1]
		db[self._table].insert_one(data)


class User(Document):
	_table='users'
	username=""
	password=""
	token=""
class Menu(Document):
	_table='menu'
	name={'value':""}
	url={'value':""}

class News(Document):
	_table='news'
	featured={'hidden':True, 'type':Document.Boolean,'input':Document.Checkbox,'label':'Mostrar en Portada', 'value':"off"}
	title={'type':Document.String,'input':Document.TextField,'label':'Título', 'value':""}
	description={ 'type':Document.String,'input':Document.TextArea,'label':'Descripción', 'value':""}

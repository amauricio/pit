from flask import Blueprint, jsonify, request, redirect, url_for, session
import bcrypt
from app.app_context import home_controller, admin_controller, news_controller, content_controller
from app.mongo import MongoConnection

main = Blueprint('main', __name__)


db_mongo = MongoConnection()

# TODO this does not work
@main.route('/', methods=['GET'])
def home():
	home_controller.database = db_mongo
	return home_controller.view(request.args)
 

@main.route('/detail/<code>', methods=['GET'])
def detail_news(code): 
	home_controller.database = db_mongo
	return home_controller.detail(code)

@main.route('/make-comment/<code>', methods=['POST'])
def make_comment(code):
	home_controller.database = db_mongo
	home_controller.make_comment(code, request.form)
	return redirect( url_for('main.detail_news', code=code) )

@main.route('/status/featured', methods=['POST'])
def featured_status():
	home_controller.database = db_mongo
	home_controller.update_featured(request.form)
	return ''

@main.route('/admin', methods=['GET', 'POST'])
def admin():
	admin_controller.database = db_mongo

	if request.method == 'POST':
		username = request.form.get('username')
		password = request.form.get('password')
		access = admin_controller.attempt( username, password )
		if access['status']:
			session['user'] = access
			return redirect( url_for('main.dashboard') )
		else:
			print('not')

	if 'user' in session:
		return redirect( url_for('main.dashboard') )
	return admin_controller.login()


@main.route('/admin/dashboard', methods=['GET', 'POST'])
def dashboard(): 
	return admin_controller.dashboard()

@main.route('/admin/news', methods=['GET', 'POST'])
def news(): 
	news_controller.database = db_mongo
	return news_controller.view()
@main.route('/admin/content')
def content():
	content_controller.database = db_mongo
	return content_controller.view()
	
@main.route('/admin/content/add-menu', methods=['POST'])
def add_menu():
	content_controller.database = db_mongo
	content_controller.add_menu(request.form)
	return redirect( url_for('main.content') )

@main.route('/admin/news/add', methods=['GET' ,'POST'])
def add_news():
	news_controller.database = db_mongo
	if request.method == 'POST':
		news_controller.save_item(request.form)
		return redirect( url_for('main.news') )

	return news_controller.add_view()

@main.route('/admin/news/edit/<code>', methods=['GET' ,'POST'])
def edit_news(code):
	news_controller.database = db_mongo
	return news_controller.edit_view(code)
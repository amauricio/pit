FROM python:3.6
ADD . /webapp
WORKDIR /webapp
RUN pip install -r requirements.txt